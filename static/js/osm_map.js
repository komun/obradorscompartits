var $jq = jQuery.noConflict();
var mymap;
$jq(window).on('map:init', function(e) {
  mymap = e.originalEvent.detail.map;
});

$jq(document).ready(function(e) {
    var checkExist = setInterval(function() {

     if ($jq('.leaflet-draw-draw-marker').length) {
        console.log("Exists!");

        location_textarea =  $jq('#id_location')[0]
        location_textarea.cols=60
        location_textarea.rows=1
        // By default we enable the draw marker
        $jq('.leaflet-draw-draw-marker')[0].click()
        // We hide the move/edit button which is not so usable
        $jq('.leaflet-draw-edit-edit')[0].hidden = true;

        clearInterval(checkExist);

     }
  }, 100);
  //This helps to avoid the https://stackoverflow.com/questions/36246815/data-toggle-tab-does-not-download-leaflet-map/36257493#36257493 error.
  setInterval(function() {
    mymap.invalidateSize();
  }, 3000);


});
