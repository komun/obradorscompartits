DEBUG = True

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = "mail.xxxx.net"
EMAIL_HOST_USER = "xxxx"
EMAIL_HOST_PASSWORD = "xxxxx"
EMAIL_PORT = 465
EMAIL_USE_SSL = True
EMAIL_USE_TLS = False
DEFAULT_FROM_EMAIL = "ObradorsCompartits.cat <xxxx@xxxx.net>"


SECRET_KEY = "xxxxxxxxxxxxxxxxxxxx"
ADMINS_EMAIL = ['xxxxxx@xxxxx.es', 'xxxx@xxxxx.cat']

DATABASES = {
     'default': {
         'ENGINE': 'django.contrib.gis.db.backends.postgis',
         'NAME': 'gis_xxxxxxxx',
         'USER': 'postgres',
         'PASSWORD': 'xxxxxxxxxx',
         'HOST': 'localhost',
         'PORT': '5432'
     }
 }
GOOGLE_MAPS_V3_APIKEY = "xxxxxxxxxxx"