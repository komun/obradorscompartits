"""tornallom URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from mapPointCriterias import views
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.i18n import JavaScriptCatalog


urlpatterns = [
    path("", views.frontend_view, name="index"),
    path(
        "accounts/register/",
        views.RegistrationView.as_view(),
        name="django_registration_register",
    ),
    path("accounts/", include("django_registration.backends.one_step.urls")),
    # path('accounts/', include('django_registration.backends.activation.urls')),
    path("accounts/", include("django.contrib.auth.urls")),
    # path('accounts/', include('registration.backends.simple.urls')), #automatic activation of users
    #    path('accounts/', include('registration.backends.default.urls')), #Registration with email confirmation
    path("admin/", admin.site.urls),
    path("baton/", include("baton.urls")),
    path("api/<str:endpoint>/", views.api_endpoints),
    path("jsi18n/", JavaScriptCatalog.as_view(), name="javascript_catalog"),
    path("pagina/<str:pagename>/", views.page_endpoints),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#

if settings.DEBUG:
    import os.path

    # settings.STATICFILES_DIRS += os.path.join(settings.BASE_DIR, "frontend","static"),
    # print('settings.STATICFILES_DIRS', settings.STATICFILES_DIRS)
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    #
    FRONTEND_URL = "/static/frontend/"
    FRONTEND_ROOT = os.path.join(settings.BASE_DIR, "frontend", "static")
    print("FRONTEND_ROOT", FRONTEND_ROOT)
    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(FRONTEND_URL, document_root=FRONTEND_ROOT)
