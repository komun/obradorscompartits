from django import forms
from .models import (
    ObradorCompartitPoint,
    Criteria,
    EvaluationCriteriasForMapPoint,
    PointStatusChoices,
)
from django.contrib.gis import forms as geoforms


# class CustomPointWidget(geoforms.OSMWidget):
#     map_width = 600
#     map_height = 400
#     default_lat = 41.3616
#     default_lon = 2.1025
#     default_zoom = 8
#     display_raw = True
#     # map_srid = 3857
#     srid = 4326
#     display_wkt = False
#     is_point = True


class ObradorCompartitPointForm(forms.ModelForm):
    # location = geoforms.PointField(label='Coordenades per al mapa', widget=CustomPointWidget)
    # titularitat_claus =  forms.ChoiceField(widget=forms.RadioSelect)
    # def __init__(self, instance, *args, **kwargs):
    #     super().__init__(instance=instance, *args, **kwargs)
    #     if instance:
    #         print("deleting field slug")
    #         del self.fields["slug"]
    #
    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     for bound_field in self:
    #         if hasattr(bound_field, "field") and bound_field.field.required:
    #             bound_field.field.widget.attrs["required"] = "required"

    class Meta:
        model = ObradorCompartitPoint
        widgets = {
            "machinery_others": forms.Textarea(
                attrs={
                    "cols": 70,
                    "rows": 3,
                    "placeholder": "Indiqueu altres electrodomèstics, eines de cuina o maquinària específica per l'especialització que té l'obrador (és a dir, màquines i eines molt concretes. Per ex: embotelladora i dipòsits inox si es fan begudes)",
                }
            ),
            "new_people_criteria": forms.Textarea(
                attrs={
                    "cols": 70,
                    "rows": 3,
                    "placeholder": "Exemples: per distància, per procedència matèries primeres, per criteris de comerç just, per tipologia d'empresa, aspectes més polítics...",
                }
            ),
        }
        exclude = []

    under_construction_fields = [
        "intro",
        "spaces",
        "main_category",
        "admin_name",
        "admin_contact",
        "admin_email",
        "admin_phone",
        "editor_consent_date",
        "editor_phone",
        "editor_name",
        "editor_email",
        "location",
    ]
    completed_required_fields = [
        "obrador_type",
        "admin_web",
        "address",
        "is_agroecologic",
        "ecologic_certified",
        "management_type",
        "participation_type",
        "intercooperation",
        "can_bring_machinery",
        "titularitat_claus",
    ]

    def clean(self):
        data = self.data

        failing_fields = {}

        if "_make-reviewing" in data.keys() or (
            "status" in self.data
            and self.data["status"] == PointStatusChoices.PUBLISHED
            and ("_save" in data.keys() or "_continue" in data.keys())
        ):
            if "under_construction" in self.data:
                required_fields = self.under_construction_fields
            else:
                required_fields = (
                    self.under_construction_fields + self.completed_required_fields
                )

            for field in required_fields:
                if field not in self.data.keys() or not self.data[field]:
                    failing_fields[field] = ["Camp requerit per a publicació!"]

            if "under_construction" not in self.data:
                # Checking if criterias are all evaluated
                for field in self.data.keys():
                    if (
                        field.startswith("criteria_points-")
                        and self.data[field] == "none"
                    ):
                        failing_fields["editor_consent_date"] = [
                            "Alguns criteris no han sigut puntuats."
                        ]

        if failing_fields:
            # self.data["form_clean"] = False
            raise forms.ValidationError(failing_fields)

    # Guardem les dades dels criteris per a que no perdre-los
    def save_criterias_form(self, request):
        criterias = Criteria.objects.all().order_by("my_order")
        for cri in criterias:
            my_order = cri.my_order

            criteria_answers = EvaluationCriteriasForMapPoint.objects.filter(
                criteria=cri, mappoint=self.instance
            )
            if criteria_answers:
                answer = criteria_answers.first()
            else:
                answer = EvaluationCriteriasForMapPoint(
                    criteria=cri, mappoint=self.instance
                )

            answer.notes = self.data[f"criteria_comment-{my_order}"]
            value = self.data[f"criteria_points-{my_order}"]
            answer.overal_points = value if value != "none" else None

            answer.save()
