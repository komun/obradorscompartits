# -*- coding: utf-8 -*-

from django.db import models
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.contrib.gis.db import models as gis_models
from django.contrib.postgres.fields import ArrayField

from taggit.managers import TaggableManager


class CharFieldWithBigTextarea(models.TextField):
    def formfield(self, **kwargs):
        kwargs.update({"widget": forms.Textarea(attrs={"rows": 50, "cols": 100})})
        return super().formfield(**kwargs)


class CharFieldWithTextarea(models.CharField):
    def formfield(self, **kwargs):
        kwargs.update({"widget": forms.Textarea(attrs={"rows": 3, "cols": 100})})
        return super().formfield(**kwargs)


class CharFieldWithRadioForm(models.CharField):
    def formfield(self, **kwargs):
        kwargs.update(
            {"widget": forms.ChoiceField(widget=forms.RadioSelect(is_hidden=False))}
        )
        return super().formfield(**kwargs)


class ChoiceArrayField(ArrayField):
    """
    A field that allows us to store an array of choices.

    Uses Django 1.9's postgres ArrayField
    and a MultipleChoiceField for its formfield.

    Usage:

        choices = ChoiceArrayField(models.CharField(max_length=...,
                                                    choices=(...,)),
                                   default=[...])
    """

    def formfield(self, **kwargs):
        defaults = {
            "form_class": forms.MultipleChoiceField,
            "choices": self.base_field.choices,
            "label": self.base_field.verbose_name,
        }
        defaults.update(kwargs)
        # Skip our parent's formfield implementation completely as we don't
        # care for it.
        # pylint:disable=bad-super-call
        return super(ArrayField, self).formfield(**defaults)

    #
    # def __init(self, label):
    #     self.formfield['label'] = label


# #Status of Point of Interest
# class PointStatus(models.Model):
#     name = models.CharField(_(u'Nom'), max_length=30)
#     codename = models.CharField(_(u'Codename'), max_length=20, unique=True)#regexp username
#     status = models.CharField(_(u'Estat'), max_length=15, default=PointTypeChoices.DRAFT, choices=PointTypeChoices.choices, null=True)
#     #next = models.ForeignKey('PointStatus', verbose_name=_(u'Següent estat:'), null=True, blank=True, on_delete=models.DO_NOTHING)
#     #description = CharFieldWithTextarea(_(u'Descripció'), max_length=500, null=True, blank=True)
#
#     class Meta:
#         verbose_name=_(u'Estat')
#         verbose_name_plural = _(u'Estats')
#         ordering=['name']
#
#     def __str__(self):
#         return self.name
#
#
# class Role(models.Model):
#     name = models.CharField(_(u'Nom'), max_length=250) #que sea codename como usuario, regex?
#     description = CharFieldWithTextarea(_(u'Descripció'), max_length=500, null=True, blank=True)
#
#     class Meta:
#         verbose_name=_(u'Rol')
#         verbose_name_plural = _(u'Rols')
#         ordering=['name']
#
#     def __str__(self):
#         return self.name
#


CATALAN = "ca"
CASTELLANO = "es-ES"

LANGUAGE_CHOICES = (
    (CATALAN, _(u"Català")),
    (CASTELLANO, _(u"Castellano")),
)
#
# class Community(models.Model):
#     name = models.CharField(_(u'Nom'), max_length=250)
#     description = CharFieldWithTextarea(_(u'Descripció'), max_length=500, null=True, blank=True)
#
#     class Meta:
#         verbose_name=_(u'Comunitat')
#         verbose_name_plural = _(u'Comunitats')
#         ordering=['name']
#
#     def __str__(self):
#         return self.name

# class Language(models.Model):
#     name = models.CharField(_(u'Nom'), max_length=20)
#     i10n_code = models.CharField(_(u'Codi i10n'), max_length=20, null=True, blank=True)
#
#     class Meta:
#         verbose_name=_(u'Idioma')
#         verbose_name_plural = _(u'Idiomes')
#         ordering=['name']
#
#     def __str__(self):
#         return self.name

#
# class UserProfile(User):
#     user = models.OneToOneField(User, on_delete=models.CASCADE, parent_link=True)
#     #language = models.CharField(_(u'Idioma interfaç'), max_length=15, choices=LANGUAGE_CHOICES, default=CATALAN, help_text=_(u"El idioma de l'interfaç."))
#     notes = CharFieldWithTextarea(_(u'Notes'), max_length=500, null=True, blank=True)
#     photograph = models.ImageField(upload_to='user_profiles', null=True, blank=True, verbose_name=_(u'Fotografia'), help_text=_(u'Dimensiones mínimas recomendadas: 200 x 100px.'), default='user_profiles/default.png')
#     #community = models.ManyToManyField(Community, verbose_name=_(u'Comunitats'), blank=True)
#
#
#     #role = models.ManyToManyField(Role, verbose_name=_(u'Roles'))
#     #ciudad = models.ForeignKey('Poblacion', verbose_name=_(u'Población'), null=True, blank=True)
#     #phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message=_(u"El número de teléfono tiene que tener el formato: '+1234567890' y una longitud máxima de 15 caracteres."))
#
#     # def usuarios(self):
#     #     return ";\n".join([u.username for u in self.users.all()])
#     # usuarios.short_description= _(u'Usuarios')
#     class Meta:
#         verbose_name=_(u'Perfil responsable')
#         verbose_name_plural = _(u'Perfils responsables')
#         ordering=['user']
#
#     def __str__(self):
#         return "<usr: {}>".format(self.username)

#
# def save_m2m(self):
#     self.instance.user_set.set(self.cleaned_data['users'])
#

# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         print('create_user_profile', instance)
#         up = UserProfile.objects.create(user=instance)
#         instance.userprofile = up
#         instance.save()
#
# class LandLevel_1(models.Model):
#     name = models.CharField(_(u'Nom'), max_length=250)
#     class Meta:
#         verbose_name=_(u'Provincia')
#         verbose_name_plural = _(u'Provincies')
#         ordering=['name']
#
#
#     def __str__(self):
#         return self.name
#
# class LandLevel_2(models.Model):
#     name = models.CharField(_(u'Nom'), max_length=250)
#     parent = models.ForeignKey(LandLevel_1, verbose_name=_(u'Provincia'), on_delete=models.CASCADE)
#     class Meta:
#         verbose_name=_(u'Comarca')
#         verbose_name_plural = _(u'Comarques')
#         ordering=['name']
#
#     def __str__(self):
#         return self.name
#
# class LandLevel_3(models.Model):
#     name = models.CharField(_(u'Nom'), max_length=250)
#     parent = models.ForeignKey('LandLevel_2', verbose_name=_(u'Comarca'), on_delete=models.CASCADE)
#     class Meta:
#         verbose_name=_(u'Municipi')
#         verbose_name_plural = _(u'Municipis')
#         ordering=['name']
#
#     def __str__(self):
#         return self.name


class Category(models.Model):
    name = models.CharField(_(u"Nom"), max_length=250)
    slug = models.SlugField(max_length=200)
    parent = models.ForeignKey(
        "Category",
        verbose_name=_(u"Subcategoria de:"),
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    description = CharFieldWithTextarea(
        _(u"Descripció"), max_length=500, null=True, blank=True
    )
    icon = models.ImageField(
        upload_to="category_icons",
        verbose_name=_(u"Icona"),
        help_text=_(u"Dimensions minimes: 40 x 40px."),
        default="category_icons/default.png",
    )
    # mapicon = models.ImageField(upload_to='category_mapicons', verbose_name=_(u'Icona del mapa'), help_text=_(u'Dimensions minimes: 40 x 40px.'), default='category_mapicons/default.png')
    my_order = models.PositiveIntegerField(
        _(u"Ordre"), default=0, blank=False, null=False
    )

    class Meta:
        verbose_name = _(u"Clau Sanitària")
        verbose_name_plural = _(u"Claus Sanitàries")
        ordering = ["my_order"]

    def __str__(self):
        return self.name


class EvaluationCriteriasForMapPoint(models.Model):
    notes = CharFieldWithTextarea(_(u"Notes"), max_length=3000, null=True, blank=True)
    dont_apply = models.BooleanField(_(u"No aplica"), null=True)
    # cambiar esto por relacion de 1:N puntos
    overal_points = models.FloatField(null=True, blank=True)
    mappoint = models.ForeignKey(
        "ObradorCompartitPoint", on_delete=models.CASCADE, related_name="evaluations"
    )
    criteria = models.ForeignKey(
        "Criteria", on_delete=models.CASCADE, related_name="evaluations"
    )


class Criteria(models.Model):
    name = models.CharField(_(u"Nom"), max_length=250)
    slug = models.SlugField(_(u"Nom url"), max_length=200)  # autoguardar en Save.
    description = CharFieldWithTextarea(
        _(u"Descripció"), max_length=1000, null=True, blank=True
    )
    question = CharFieldWithTextarea(
        _(u"Pregunta"), max_length=1000, null=True, blank=True
    )
    icon = models.ImageField(
        upload_to="images/criteria_icons",
        verbose_name=_(u"Icona"),
        help_text=_(u"Dimensió mínima: 40 x 40px."),
        default="criteria_icons/default.png",
    )
    my_order = models.PositiveIntegerField(
        _(u"Ordre"), default=0, blank=False, null=False
    )

    class Meta:
        verbose_name = _(u"Criteri")
        verbose_name_plural = _(u"Criteris")
        ordering = ["my_order"]

    def __str__(self):
        return self.name


#
# class Comment(models.Model):
#     text = models.CharField(_(u'Text'), max_length=3000, null=True)
#     author = models.ForeignKey('UserProfile', verbose_name=_(u'Autor/a'), on_delete=models.SET_NULL,  null=True)
#     published_date = models.DateTimeField(_(u'Data Publicació'), auto_now_add=True)
#     mappoint = models.ForeignKey('MapPoint', verbose_name=_(u'Punt'), on_delete=models.CASCADE, related_name="point_comments")
#
#     class Meta:
#         verbose_name=_(u'Comentari')
#         verbose_name_plural = _(u'Comentaris')
#         ordering=['published_date']
#
#     def __str__(self):
#         return "{}:{}".format(self.published_date, self.author)
#
#     def save(self, *args, **kwargs):
#         if not self.id:
#             if not self.author:
#                 # print('Object is a new instance from form without author, ignoring since it is saved in admin overwrite')
#                 return
#
#         return super(Comment, self).save(*args, **kwargs)


class LinkType(models.TextChoices):
    WEBSITE = "WEBSITE", _(u"Lloc Web")
    FACEBOOK = "FACEBOOK", _(u"Facebook")
    TWITTER = "TWITTER", _(u"Twitter")
    INSTAGRAM = "INSTAGRAM", _(u"Instagram")
    TELEGRAM = "TELEGRAM", _(u"Telegram")
    WHATSAAP = "WHATSAAP", _(u"Whatsapp")
    YOUTUBE = "YOUTUBE", _(u"Youtube")
    PINTEREST = "PINTEREST", _(u"Pinterest")
    MASTODON = "MASTODON", _(u"Mastodon")
    OTHER = "OTHER", _(u"Altre")


#
# class PointLink(models.Model):
#     point = models.ForeignKey('MapPoint', verbose_name=_(u'Punt'), on_delete=models.CASCADE, related_name="point_links")
#     link_type = models.CharField(_(u"Tipus d'enllaç"),  max_length=10, choices=LinkType.choices, default=LinkType.WEBSITE, null=False, blank=False)
#     url = models.URLField(_(u"Enllaç"), null=False, blank=False)


class PointImage(models.Model):
    point = models.ForeignKey(
        "ObradorCompartitPoint",
        verbose_name=_(u"Punt"),
        on_delete=models.CASCADE,
        related_name="point_images",
    )
    picture = models.ImageField(
        upload_to="point_images",
        verbose_name=_(u"Imatge"),
        help_text=_(u"Dimensions mínimes recomendades: 200 x 100px."),
        default="point_images/default.jpg",
    )


class PointMember(models.Model):
    point = models.ForeignKey(
        "ObradorCompartitPoint",
        verbose_name=_(u"Punt"),
        on_delete=models.CASCADE,
        related_name="point_members",
    )
    picture = models.ImageField(
        upload_to="point_images",
        verbose_name=_(u"Imatge"),
        help_text=_(u"Dimensions mínimes recomendades: 200 x 200px."),
        default="point_images/default.jpg",
    )
    name = models.CharField("Nom projecte/empresa/elaboradora", max_length=100)
    web = models.URLField(
        _(u"Web (o URL xarxa social principal)"), null=False, blank=True
    )
    town = models.CharField(
        "Població",
        max_length=250,
        null=True,
        blank=True,
    )
    work_comment = CharFieldWithTextarea(
        "Elaboracions que fa (només tipologia, productes finals, marques...)",
        max_length=250,
        null=True,
        blank=True,
    )


class TrinaryBoolean(models.TextChoices):
    YES = "YES", _(u"Si")
    NO = "NO", _(u"No")
    NOT_APPLY = "NOT_APPLY", _(u"No Aplica")


class TitularitatChoices(models.TextChoices):
    PROPIA = "ELABORADORA", _(u"Cada elaboradora ha de tenir la seva")
    ESPAI = "ESPAI", _(u"L'espai posa les claus per tothom")


class ObradorTypeChoices(models.TextChoices):
    PUBLIC = "PUBLIC", _(u"Públic")
    PRIVAT = "PRIVAT", _(u"Privat")
    SOCIAL = "SOCIAL", _(
        u"De capital social (associacions, cooperatives, altres formes jurídiques sense ànim de lucre, etc.)"
    )
    MIXT = "MIXT", _(u"Mixt")


class CanMakeEcologicChoices(models.TextChoices):
    YES = "SI", _(u"Sí")
    NO = "NO", _(u"No")


class BooleanChoices(models.TextChoices):
    YES = "SI", _(u"Sí")
    NO = "NO", _(u"No")


class CertifiedTypeChoices(models.TextChoices):
    NO = "NO", "NO. Es ecològic NO certificat"
    SI_CCPAE = "SI_CCPAE", "Ecològic certificat pel CCPAE o equivalent"
    SI_SPG = "SI_SPG", "Ecològic certificat per SPG (Sistema Participatiu de Garantia)"


class PriorityDisplayChoices(models.TextChoices):
    SOCIAL = "SOCIAL", "Capital social"
    ECOLOGIC = "ECOLOGIC", "Agroecològic"


class ManagementChoices(models.TextChoices):
    PROPIETAT = (
        "PROPIETAT",
        "Gestió per part de la propietat (sigui pública, privada o de capital social)",
    )
    EXTERNA = "EXTERNA", "Gestió per part d'una empresa externa"
    COMUNITARIA = "COMUNITARIA", "Gestió comunitària"


class ParticipationChoices(models.TextChoices):
    COLECTIVA = "COLECTIVA", "Gestió col·lectiva-comunitària"
    ASSEMBLEA = "ASSEMBLEA", "Assemblea-reunió d'elaboradores periòdica"
    PUNTUALS = "PUNTUALS", "Reunions puntuals o sota demanda"
    NO_PODEN = (
        "NO_PODEN",
        "Les elaboradores no poden participar en decisions globals de l'espai",
    )


class TogetherProjectsChoices(models.TextChoices):
    NO = "NO", "No es fan projectes conjunts"
    SI_PERO_NO = "SI_PERO_NO", "S'incentiven però actualment no se'n duen a terme"
    APROVISIONAMENT = "APROVISIONAMENT", "Aprovisionament (compres conjuntes)"
    MARQUETING = "MARQUETING", "Comercialització, màrqueting, marca conjunta."
    CONJUNTES = "CONJUNTES", "Produccions conjuntes"
    FORMACIONS = "FORMACIONS", "Formacions"


class SpacesChoices(models.TextChoices):
    ELABORACIO = "ELABORACIO", "Sala d'elaboració"
    ELABORACIO2 = "ELABORACIO2", "Una segona (o més) sala d'elaboració"
    ALTRES_FASES = (
        "ALTRES_FASES",
        "Altres sales per fases concretes de les elaboracions (cocció, rentat, manipulació..)",
    )
    MACERACIO = (
        "MACERACIO",
        "Sala de maceració-curació (per productes que han de quedar-se un temps acabant de 'madurar')",
    )
    EXPEDICIO = "EXPEDICIO", "Sala d'expedició"
    MAGATZEM = "MAGATZEM", "Magatzem"
    COWORKING = (
        "COWORKING",
        "Espais de coworking (despatxos, sala de reunions, cuina-office..)",
    )


class MachineryChoices(models.TextChoices):
    NEVERA = "NEVERA", "Nevera"
    CONGELADOR = "CONGELADOR", "Congelador"
    CAMBRA_FRIGO = "CAMBRA_FRIGO", "Cambra frigorífica"
    ABAT_TEMPERATURA = "ABAT_TEMPERATURA", "Abatedor de temperatura"
    PAUSTERITZADORA = "PAUSTERITZADORA", "Pasteuritzadora"
    FORN = "FORN", "Forn"
    FOGO_ESTANDARD = "FOGO_ESTANDARD", "Fogons estandard"
    FOGO_INDUSTRIAL = "FOGO_INDUSTRIAL", "Fogó/ns industrial/s"
    BANY_MARIA = "BANY_MARIA", "Bany maria electrònic"
    AUTOCLAU = "AUTOCLAU", "Autoclau"
    ENV_BUIT = "ENV_BUIT", "Envasadora al buit"
    FREGIDORA = "FREGIDORA", "Fregidora"
    RENTAPLATS = "RENTAPLATS", "Rentavaixelles"
    BALANZA = "BALANZA", "Balança"
    ETIQUETADORA = "ETIQUETADORA", "Etiquetadora"


class CanBringMachineryChoices(models.TextChoices):
    SI = "SI", "Sí"
    NO = "NO", "No"
    DEPENDE = "DEPENDE", "S'ha de mirar cas a cas"


class PointStatusChoices(models.TextChoices):
    DRAFT = "DRAFT", "Esborrany"
    REVIEW_PENDING = "REVIEW_PENDING", "Pendent de revisió"
    PUBLISHED = "PUBLISHED", "Publicat"
    ARCHIVED = "ARCHIVED", "Arxivat"


# https://realpython.com/location-based-app-with-geodjango-tutorial/#adding-geodjango
class MapPointBasic(models.Model):
    under_construction = models.BooleanField(
        "En construcció",
        help_text="Marqueu si encara no està en funcionament (perquè encara està en obres o perquè encara és d'ús exclusiu d'un sol projecte elaborador)",
        default=False,
        null=False,
        blank=False,
    )
    # Informació persona que omple la fitxa
    editor_name = models.CharField(
        "Nom persona que omple la fitxa", max_length=250, null=True, blank=True
    )
    editor_role = models.CharField(
        _("Càrrec/Responsabilitat"), max_length=80, null=True, blank=True
    )
    editor_email = models.EmailField(_(u"Correu electrònic"), null=True, blank=True)
    editor_phone = models.CharField(_(u"Telèfon"), max_length=40, null=True, blank=True)
    editor_consent_date = models.DateField(
        _(u"Consent política de dades"), null=True, blank=True
    )
    editor = models.ForeignKey(
        User,
        verbose_name=_(u"Editor/a Responsable"),
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    # Informació general obrador
    name = models.CharField("Nom", max_length=250)

    intro = CharFieldWithTextarea(
        _(u"Breu descripció"), max_length=500, null=True, blank=True
    )
    # body = CharFieldWithTextarea(_(u'Breu descripció'), max_length=500, null=True, blank=True)
    tags = TaggableManager(
        verbose_name=_("Etiquetes"),
        help_text=_("Una llista d'etiquetes separada per comes."),
        blank=True,
    )
    location = gis_models.PointField(
        "Coordenades per al mapa", null=True, blank=True, geography=True, srid=4326
    )
    address = models.CharField("Direcció postal", max_length=100, null=True, blank=True)

    slug = models.SlugField(
        _(u"Nom url"),
        help_text="S'utilitzará per a la url pública de l'obrador",
        max_length=200,
        null=True,
        blank=True,
    )
    picture = models.ImageField(
        upload_to="point_images",
        verbose_name=_(u"Logotip/Foto"),
        help_text=_(u"Dimensions mínimes recomendades: 200 x 100px."),
        default="point_images/default.jpg",
        blank=True,
    )
    phone = models.CharField(
        _(u"Telèfon"), max_length=40, null=True, blank=True
    )  # regexp username
    web = models.URLField(_(u"Web"), null=True, blank=True)
    created_date = models.DateField(_(u"Data Publicació"), auto_now_add=True)
    modified_date = models.DateTimeField(_(u"Data Modificació"), auto_now=True)

    # Informació general gestió
    admin_name = models.CharField(
        "Nom entitat/empresa", max_length=250, null=True, blank=True
    )
    admin_logo = models.ImageField(
        upload_to="point_images",
        verbose_name=_(u"Logotip"),
        help_text=_(u"Dimensions mínimes recomendades: 200 x 100px."),
        default="point_images/default.jpg",
        blank=True,
    )
    admin_web = models.URLField(_(u"Web"), null=True, blank=True)
    admin_contact = models.CharField(
        "Persona de contacte", max_length=250, null=True, blank=True
    )
    admin_email = models.EmailField(_(u"Correu electrònic"), null=True, blank=True)
    admin_phone = models.CharField(_(u"Telèfon"), max_length=40, null=True, blank=True)

    status = models.CharField(
        _(u"Estat del formulari"),
        max_length=15,
        default=PointStatusChoices.DRAFT,
        choices=PointStatusChoices.choices,
        null=True,
    )

    class Meta:
        abstract = True


class EspaiTestAgrariPoint(MapPointBasic):
    class Meta:
        verbose_name = _(u"Espai Test Agrari")
        verbose_name_plural = _(u"Espais Test Agraris")
        ordering = ["created_date"]

    def __str__(self):
        return self.name


class EntitatSuportPoint(MapPointBasic):
    class Meta:
        verbose_name = _(u"Entitat de Suport")
        verbose_name_plural = _(u"Entitats de Suport")
        ordering = ["created_date"]

    def __str__(self):
        return self.name


class ObradorCompartitPoint(MapPointBasic):

    # Informació específica
    obrador_type = models.CharField(
        "Tipus d'obrador a nivell de titularitat",
        max_length=20,
        choices=ObradorTypeChoices.choices,
        default=None,
        null=True,
        blank=True,
    )
    is_kickstarter = models.BooleanField(
        "L'obrador és un viver?",
        help_text="és a dir, que està pensat per ajudar a impulsar iniciatives noves i durant un període limitat de temps.",
        default=False,
        null=False,
        blank=False,
    )
    is_agroecologic = models.CharField(
        "Us considereu un obrador agroecològic?",
        help_text="és a dir, que us considereu un projecte polític i teniu en compte aspectes socials i econòmics més enllà de l'ecologia.",
        max_length=20,
        choices=CanMakeEcologicChoices.choices,
        default=CanMakeEcologicChoices.NO,
        null=True,
        blank=True,
    )
    ecologic_certified = models.CharField(
        "A l'obrador es pot elaborar en ecològic certificat?",
        max_length=20,
        help_text="Certificat l'obrador o alguna de les seves elaboradores, cosa que ho fa més fàcil per a noves elaborades que vulguin certificar-se.",
        choices=BooleanChoices.choices,
        default=BooleanChoices.NO,
        null=True,
        blank=True,
    )
    # priority_display = models.CharField(
    #     "Si l'obrador és de capital social i també agroecològic, què prioritzaries destacar-ne al mapa?",
    #     help_text="Depenent de l'elecció, el color que envoltarà el punt en el mapa serà verd si s'ha triat ecològic, o lila si s'ha triat capital social.",
    #     max_length=20,
    #     choices=PriorityDisplayChoices.choices,
    #     default=None,
    #     null=True,
    #     blank=True,
    # )
    is_blanc = models.BooleanField(
        "L'obrador ofereix servei d'obrador blanc?",
        help_text="Sigui una elaboradora concreta o l'obrador col·lectivament.",
        default=False,
        null=False,
        blank=False,
    )

    management_type = models.CharField(
        "Forma de gestió",
        max_length=20,
        choices=ManagementChoices.choices,
        default=None,
        null=True,
        blank=True,
    )

    participation_type = models.CharField(
        "Es promou la participació de les elaboradores?",
        max_length=20,
        choices=ParticipationChoices.choices,
        default=None,
        null=True,
        blank=True,
    )

    intercooperation = ChoiceArrayField(
        models.CharField(
            "Incentiveu i es realitzen projectes conjunts entre les elaboradores? De quin tipus?",
            max_length=20,
            choices=TogetherProjectsChoices.choices,
        ),
        help_text="Manteniu la tecla CTRL presionada per a seleccionar múltiples opcions.",
        default=list,
        null=True,
        blank=True,
    )

    intercooperation_others = models.CharField(
        "Altres tipus de projectes conjunts", max_length=250, null=True, blank=True
    )

    spaces = ChoiceArrayField(
        models.CharField(
            "De quins espais disposeu?",
            max_length=20,
            choices=SpacesChoices.choices,
        ),
        default=list,
        null=True,
        blank=True,
    )

    spaces_others = models.CharField(
        "Teniu altres espais?", max_length=250, null=True, blank=True
    )

    machinery = ChoiceArrayField(
        models.CharField(
            "De quina maquinària disposeu?",
            max_length=20,
            choices=MachineryChoices.choices,
        ),
        default=list,
        null=True,
        blank=True,
    )

    machinery_others = models.CharField(
        "Teniu altra maquinària?", max_length=250, null=True, blank=True
    )

    can_bring_machinery = models.CharField(
        "Les elaboradores poden portar maquinària pròpia?",
        max_length=20,
        choices=CanBringMachineryChoices.choices,
        default=None,
        null=True,
        blank=True,
    )

    new_people_criteria = models.CharField(
        "Hi ha criteris d'accés per a noves elaboradores? Quins? Més enllà de complir amb la tipologia de productes i possibilitats de l'espai.",
        max_length=500,
        null=True,
        blank=True,
    )

    # claus
    titularitat_claus = models.CharField(
        "Titularitat claus sanitàries",
        max_length=20,
        choices=TitularitatChoices.choices,
        default=None,
        null=True,
        blank=True,
    )

    other_categories = models.ManyToManyField(
        Category, verbose_name="Quines claus teniu actualment?", blank=True
    )
    main_category = models.ForeignKey(
        "Category",
        verbose_name="Quina d'aquestes és la principal?",
        help_text="La que voleu destacar, la més utilitzada i/o per la que disposeu de més maquinària específica. Aquesta clau serà la que determinarà la icona utilitzada al mapa.",
        related_name="points",
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True,
    )

    want_more_categories = models.BooleanField(
        "Esteu oberts a obtenir altres claus?  Independentment de la titularitat de les claus del vostre obrador.",
        default=False,
        null=False,
        blank=False,
    )

    category_comments = CharFieldWithTextarea(
        _(u"Comentaris/Aclariments"), max_length=500, null=True, blank=True
    )

    # Membres
    total_members = models.PositiveIntegerField(
        "Quantes elaboradores (quants projectes diferents) fan servir l'obrador actualment?",
        default=0,
        blank=False,
        null=False,
    )

    class Meta:
        verbose_name = _(u"Obrador Compartit")
        verbose_name_plural = _(u"Obradors Compartits")
        ordering = ["created_date"]

    def __str__(self):
        return self.name

    def get_slug_categories(self):
        other_categories = set()
        other_categories.add(self.main_category.slug)
        if self.under_construction:
            other_categories.add("obrador_pendent")
        # if self.is_social:
        #     other_categories.add("social")
        if self.is_ecologic:
            other_categories.add("agroecologic")
        for cat in self.other_categories.all():
            other_categories.add(cat.slug)
        return list(other_categories)

    def get_other_categories_slugs(self):
        other_categories = [self.main_category]
        for cat in self.other_categories.all():
            other_categories.append(cat)

        return other_categories

    @property
    def is_social(self):
        return self.obrador_type == ObradorTypeChoices.SOCIAL

    @property
    def is_ecologic(self):
        return self.is_agroecologic == CanMakeEcologicChoices.YES

    @property
    def is_ecologic_certified(self):
        return self.ecologic_certified == BooleanChoices.YES

class Page(models.Model):
    url = models.CharField(
        "URL",
        help_text="Es trobarà com a https://obradorscompartits.cat/pagina/[URL]",
        max_length=50,
        null=False,
        blank=False,
    )

    content = CharFieldWithBigTextarea(
        "Contingut HTML",
        help_text="Compatible amb jquery 3.5, FontAwesome 5.2 i Bootstrap 4.5",
        null=False,
        blank=False,
    )

    class Meta:
        verbose_name = "Pàgina"
        verbose_name_plural = "Pàgines"
        ordering = ["url"]

    def __str__(self):
        return self.url
