CATEGORIES = [
    ("carns", "c10. Carns i derivats, aus i caça"),
    ("peixos", "c12. Peixos, crustacis, mol·luscs i derivats"),
    ("ous", "c14. Ous i derivats"),
    ("llets", "c15. Llet i derivats"),
    ("olis", "c16. Oleaginoses i greixos comestibles"),
    ("cereals", "c20. Cereals, farines i derivats"),
    (
        "vegetals",
        "c21. Vegetals (hortalisses, fruites, bolets, tubercles, llegums) i derivats",
    ),
    (
        "sucres",
        "c23. Sucres, derivats, mel i productes relacionats amb la producció de la mel",
    ),
    ("especies", "c24. Condiments i espècies"),
    (
        "aliments",
        "c25. Aliments estimulants, espècies vegetals per a infusions i derivats",
    ),
    (
        "preparats",
        "c26. Menjars preparats, aliments per a grups específics, complements alimentaris i altres ingredients i productes alimentaris",
    ),
    ("gelats", "c28. Gelats"),
    ("begudes_noalcoholiques", "c29. Begudes no alcohòliques"),
    ("begudes_alcoholiques", "c30. Begudes alcohòliques"),
    ("restauracio_colectiva", "c50. Restauració col·lectiva social"),
    ("cosmetica", "Cosmètica"),
]
#
# MAPPOINTS = [
#     {"name":"Obrador A actiu","status":"Publicat", "lng": 0.6246174, "lat": 41.6888382,
#             "main_cat":"vegetals", "other_cats": ["gelats", "especies"], "under_construction": False},
#     {"name":"Obrador B en construcció","status":"Publicat", "lng": 1.146468, "lat": 41.3722001,
#             "main_cat":"begudes_alcoholiques", "other_cats": ["gelats", "begudes_noalcoholiques"],  "under_construction": True},
# ]

SIMPLE_CRITERIAS = [
    {
        "title": "Proveïdores",
        "question": "Procedència matèries primeres de proximitat",
        "description": "(0 = no es té gens en compte; 5 = és quelcom indispensable per entrar a l'obrador)",
        "icona": "criteria_icons/proveidores.png",
    },
    {
        "title": "Sostenibilitat ambiental",
        "question": "Consum de les màquines, ús d'energies renovables, reducció de residus, CO2, KM0, productes amb una producció amb baix consum de recursos...)",
        "description": "(0 = no es té gens en compte; 5 = és una prioritat de l'obrador)",
        "icona": "criteria_icons/consum-energetic.png",
    },
    {
        "title": "Arrelament territorial",
        "question": "Obertura al territori, priorització productores de proximitat, vinculació amb el poble, municipi, comarca, etc.",
        "description": "(0 = no es té gens en compte; 5 = és una prioritat de l'obrador)",
        "icona": "criteria_icons/arrelament-territorial.png",
    },
    {
        "title": "Treball en xarxa entre les productores de l'obrador",
        "question": "",
        "description": "(0 = cada productora elabora i prou; 3 = es fan projectes en xarxa; 5 = es treballa de forma comunitària)",
        "icona": "criteria_icons/desenvolupament-personal.png",
    },
    {
        "title": "Producte ecològic",
        "question": "",
        "description": "(0 = no es té gens en compte; 3 = es potencia l'ús de matèries primeres ecològiques; 5 = obrador només per elaboracions en ecològic)",
        "icona": "criteria_icons/sostenibilitat-ambiental.png",
    },
    {
        "title": "Varietats/elaboracions tradicionals/autòctones/silvestres",
        "question": "",
        "description": "(0 = no es té gens en compte; 5 = és una prioritat de l'obrador)",
        "icona": "criteria_icons/elaboracions-tradicionals.png",
    },
    {
        "title": "Condicions de treball",
        "question": "Aspectes de condicions de treball justes, tant pels tècnics de l'obrador (si n'hi ha), com de les elaboradores i de les seves proveïdores",
        "description": "(0 = no es té gens en compte; 5 = és una prioritat de l'obrador)",
        "icona": "criteria_icons/condicions-laborals.png",
    },
    {
        "title": "Criteris socials",
        "question": "Criteris socials (amb col·lectius desafavorits..)",
        "description": "(0 = no es té gens en compte; 5 = és una prioritat de l'obrador)",
        "icona": "criteria_icons/cohesio-social.png",
    },
    {
        "title": "Democràcia/governança",
        "question": "",
        "description": "(0 = relació clientelar entre la gestió de l'obrador i les elaboradores; 3 = governança participativa; 5 = gestió comunitària)",
        "icona": "criteria_icons/democracia-interna.png",
    },
    {
        "title": "Economia Social i Solidària",
        "question": "",
        "description": "(0 = no es té gens en compte; 5 = només poden entrar productores de l'ESS sense ànim de lucre)",
        "icona": "criteria_icons/finances-transformadores.png",
    },
]

STATUS = [
    {"name": "Esborrany", "codename": "ESBORRANY", "type": "DRAFT"},
    {
        "name": "Pendent de revisar",
        "codename": "PENDENTDEREVISAR",
        "type": "REVIEW_PENDING",
    },
    {"name": "Publicat", "codename": "PUBLICAT", "type": "PUBLISHED"},
    {"name": "Arxivat", "codename": "ARXIVAT", "type": "ARCHIVED"},
]

# NETWORKS = ["Associació de Societats Laborals i Economia Social de Catalunya (Asescat)",
# "Barcola (Procomú)",
# "Confederació de Cooperatives de Catalunya (CCC)",
# "Coop57",
# "Eixos comercials (Mercantils)",
# "Entitats Catalanes d’Acció Social (ECAS)",
# "Federació de Centres Especials de Treball de Catalunya (Fecetc)",
# "Federació de Cooperatives Agràries",
# "Federació de Cooperatives d'Ensenyament",
# "Federació de Cooperatives d'Habitatge",
# "Federació de Cooperatives de Consumidors i Usuaris (FCCUC)",
# "Federació de Cooperatives de Treball (FCTC)",
# "Federació de Mutualitats de Catalunya",
# "Federació d’Empreses d’Inserció de Catalunya (FEICAT)",
# "Federació d’Entitats de Discapacitat Intel·lectual de Catalunya (DINCAT)",
# "Fiare",
# "Finançament Ètic i Solidari (Fets)",
# "Gremis (Mercantils)",
# "La Confederació (organització empresarial d’entitats no lucratives de serveis d’atenció a les persones)",
# "Taula Tercer Sector Social",
# "Xarxa d'Economia Solidària",
# "Xarxa d'Espais Comunitaris"]

# PARTICIPANT_ROLES = [
# ("Persones remunerades","Quantitat de persones remunerades"),
# ("Càrrecs directius","De les persones remunerades, qui ocupa càrrecs directius o de coordinació"),
# ("Òrgans de govern","Òrgans de govern (consell rector, junta o patronat)"),
# ("Persones no remunerades","Persones no remunerades")]

# USERS = {'demo': 'mar_cabanes@yahoo.es', 'patricia': 'patriciadopazo@riseup.net', 'pedro': 'pedro@altur.coop',
#           'tereseta': 'tereseta@riseup.net', 'raquelillaginer': 'ginerraquel@gmail.com'}
