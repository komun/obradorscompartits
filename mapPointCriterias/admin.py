from django.contrib import admin
from django.contrib.admin.utils import flatten_fieldsets

# class MyAdminSite(admin.AdminSite):
#     site_url = 'https://yourdomain.com'

# Register your models here.
from adminsortable2.admin import SortableAdminMixin

from django.utils.text import slugify

from .models import (
    ObradorCompartitPoint,
    EspaiTestAgrariPoint,
    EntitatSuportPoint,
    Category,
    Criteria,
    PointImage,
    PointMember,
    Page,
)
from .forms import ObradorCompartitPointForm
from .utils import limit_list_to_owned_items, onchange_review_buttons

# BatonAdminSite.index_template = 'baton/index.html'



from leaflet.admin import LeafletGeoAdminMixin




@admin.register(Category)
class CategoryAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = (
        "name",
        "slug",
    )


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ("url",)


@admin.register(Criteria)
class CriteriaAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ("name",)
    exclude = ("slug",)
    # prepopulated_fields = {"slug": ("name",)}


#
# @admin.register(PointStatus)
# class PointStatusAdmin(admin.ModelAdmin):
#     list_display = ('name', 'type',)


class PointMemberInline(admin.TabularInline):
    # model = MapPoint.comments.through
    model = PointMember
    verbose_name = u"Elaborador/a"
    verbose_name_plural = u"Elaboradores"
    extra = 0
    max_num = 50
    # def row_name(self, instance):
    #     return instance.row.name
    # row_name.short_description = 'row name'


class PointImageInline(admin.TabularInline):
    # model = MapPoint.comments.through
    model = PointImage
    verbose_name = u"Imatge"
    verbose_name_plural = u"Galeria d'imatges/fotos"
    extra = 0
    max_num = 3


# class PointLinkInline(admin.TabularInline):
#     # model = MapPoint.comments.through
#     model = PointLink
#     verbose_name = u"Enllaç"
#     verbose_name_plural = u"Enllaços"
#     extra = 0

# @admin.register(Comment)
# class CommentAdminInline(admin.StackedInline):
# class CommentInline(admin.TabularInline):
#     # model = MapPoint.comments.through
#     model = Comment
#     verbose_name = u"Comentari"
#     verbose_name_plural = u"Comentaris"
#     extra = 1
#     form = CustomCommentForm



class CustomLeafletGeoAdmin(LeafletGeoAdminMixin, admin.ModelAdmin):
    """
    Custom Leaflet Geo Admin class to adjust coordinate display.
    """
    #map_srid = 3857  # Set the coordinate system if necessary
    srid = 4326 # Set the coordinate system if necessary
    def formfield_for_dbfield(self, db_field, request, **kwargs):
        formfield = super().formfield_for_dbfield(db_field, request, **kwargs)

        # Check if the field is the 'location' field
        if db_field.name == 'location':
            if formfield.initial:
                lat, lon = formfield.initial.coords  # assuming it's stored as (lat, lon)
                formfield.initial = (lon, lat)  # Reverse the order for display

        return formfield

@admin.register(EntitatSuportPoint)
class EntitatSuportPointAdmin(CustomLeafletGeoAdmin):
    list_display = ("name", "status", "editor")
    list_filter = ("status",)
    search_fields = ("name", "intro")
    readonly_fields = ("created_date", "modified_date")

    def get_search_results(self, request, queryset, search_term):
        return limit_list_to_owned_items(
            self, EntitatSuportPointAdmin, request, queryset, search_term
        )

    def save_model(self, request, obj, form, change):
        # print('save_model')
        if not change:
            obj.editor = request.user
            obj.slug = "xsuport_" + slugify(obj.name)
        super().save_model(request, obj, form, change)

    def get_readonly_fields(self, request, obj=None):
        dynamic_fields = []
        if not request.user.is_superuser:
            dynamic_fields = ["status", "slug"]

        return list(self.readonly_fields) + dynamic_fields

    def response_change(self, request, obj):
        onchange_review_buttons(self, request, obj, "entitatsuportpoint")

        return super().response_change(request, obj)

    # change_form_template = "admin/mappoint_changeform.html"

    class Media:
        js = ("js/osm_map.js",)

    fieldsets = (
        (
            "Espai",
            {
                "fields": (
                    "under_construction",
                    "name",
                    "slug",
                    "intro",
                    "picture",
                    "web",
                    "tags",
                ),
                "classes": (
                    "order-0",
                    "baton-tabs-init",
                    "baton-tab-fs-contact",
                    "baton-tab-fs-gestio",
                    "baton-tab-fs-location",
                ),
            },
        ),
        (
            "Editor",
            {
                "fields": (
                    "editor_name",
                    "editor_role",
                    "editor_phone",
                    "editor_email",
                ),
                "classes": ("tab-fs-contact",),
                "description": "Informació sobre qui omple la fitxa.",
            },
        ),
        (
            "Gestió",
            {
                "fields": (
                    "admin_name",
                    "admin_logo",
                    "admin_web",
                    "admin_contact",
                    "admin_email",
                    "admin_phone",
                ),
                "classes": ("tab-fs-gestio",),
                "description": "Informació de l'entitat/empresa gestionadora (o que en lidera el procés mentre està en construcció)",
            },
        ),
        (
            "Ubicació",
            {
                "fields": (
                    "address",
                    "location",
                ),
                "classes": ("tab-fs-location",),
            },
        ),
        (
            "Administració",
            {
                "fields": (
                    "created_date",
                    "modified_date",
                    "status",
                ),
                "classes": ("tab-fs-admin",),
            },
        ),
    )


@admin.register(EspaiTestAgrariPoint)
class EspaiTestAgrariPointAdmin(CustomLeafletGeoAdmin):
    list_display = ("name", "status", "editor")
    list_filter = ("status",)
    search_fields = ("name", "intro")
    readonly_fields = ("created_date", "modified_date")

    def get_search_results(self, request, queryset, search_term):
        return limit_list_to_owned_items(
            self, EspaiTestAgrariPointAdmin, request, queryset, search_term
        )

    def save_model(self, request, obj, form, change):
        # print('save_model')
        if not change:
            obj.editor = request.user
            obj.slug = "xtest_" + slugify(obj.name)
        super().save_model(request, obj, form, change)

    def get_readonly_fields(self, request, obj=None):
        dynamic_fields = []
        if not request.user.is_superuser:
            dynamic_fields = ["status", "slug"]

        return list(self.readonly_fields) + dynamic_fields

    def response_change(self, request, obj):
        onchange_review_buttons(self, request, obj, "espaitestagraripoint")

        return super().response_change(request, obj)

    # change_form_template = "admin/mappoint_changeform.html"

    class Media:
        js = ("js/osm_map.js",)

    fieldsets = (
        (
            "Espai",
            {
                "fields": (
                    "under_construction",
                    "name",
                    "slug",
                    "intro",
                    "picture",
                    "web",
                    "tags",
                ),
                "classes": (
                    "order-0",
                    "baton-tabs-init",
                    "baton-tab-fs-contact",
                    "baton-tab-fs-gestio",
                    "baton-tab-fs-location",
                    "baton-tab-fs-admin",
                ),
            },
        ),
        (
            "Editor",
            {
                "fields": (
                    "editor_name",
                    "editor_role",
                    "editor_phone",
                    "editor_email",
                ),
                "classes": ("tab-fs-contact",),
                "description": "Informació sobre qui omple la fitxa.",
            },
        ),
        (
            "Gestió",
            {
                "fields": (
                    "admin_name",
                    "admin_logo",
                    "admin_web",
                    "admin_contact",
                    "admin_email",
                    "admin_phone",
                ),
                "classes": ("tab-fs-gestio",),
                "description": "Informació de l'entitat/empresa gestionadora  (o que en lidera el procés mentre està en construcció)",
            },
        ),
        (
            "Ubicació",
            {
                "fields": (
                    "address",
                    "location",
                ),
                "classes": ("tab-fs-location",),
            },
        ),
        (
            "Administració",
            {
                "fields": (
                    "created_date",
                    "modified_date",
                    "status",
                ),
                "classes": ("tab-fs-admin",),
            },
        ),
    )


@admin.register(ObradorCompartitPoint)
class ObradorCompartitPointAdmin(CustomLeafletGeoAdmin):
    list_display = ("name", "status", "editor")
    filter_horizontal = ("other_categories",)
    list_filter = ("status",)
    search_fields = ("name", "intro")
    readonly_fields = ("created_date", "modified_date")
    # prepopulated_fields = {"slug": ["name"]}
    form = ObradorCompartitPointForm

    # radio_fields = {'titularitat_claus': admin.VERTICAL}
    inlines = [
        PointMemberInline,
        PointImageInline,
    ]

    # exclude = (,)
    fieldsets = (
        (
            "Obrador",
            {
                "fields": (
                    "under_construction",
                    "name",
                    "intro",
                    "picture",
                    "web",
                    "tags",
                ),
                "classes": (
                    "order-0",
                    "baton-tabs-init",
                    "baton-tab-fs-editor",
                    "baton-tab-fs-gestio",
                    "baton-tab-fs-criteris",
                    "baton-tab-fs-specific",
                    "baton-tab-fs-categories",
                    "baton-tab-fs-location",
                    "baton-tab-group-fs-members--inline-point_members",
                    "baton-tab-fs-admin",
                ),
                "description": "Informació general sobre l'obrador.",
            },
        ),
        (
            "Editor",
            {
                "fields": (
                    "editor_name",
                    "editor_role",
                    "editor_phone",
                    "editor_email",
                ),
                "classes": ("tab-fs-editor",),  #'tab-fs-content', ),
                "description": "Informació sobre qui omple la fitxa.",
            },
        ),
        (
            "Gestió",
            {
                "fields": (
                    "admin_name",
                    "admin_logo",
                    "admin_web",
                    "admin_contact",
                    "admin_email",
                    "admin_phone",
                ),
                "classes": ("tab-fs-gestio",),  #'tab-fs-content', ),
                "description": "Informació de l'entitat/empresa que gestiona l'obrador  (o que en lidera el procés mentre està en construcció).",
            },
        ),
        (
            "Criteris",
            {
                "fields": ("editor_consent_date",),
                "classes": ("tab-fs-criteris",),
            },
        ),
        (
            "Específic",
            {
                "fields": (
                    "obrador_type",
                    "is_kickstarter",
                    "is_agroecologic",
                    "ecologic_certified",
                    "is_blanc",
                    "management_type",
                    "participation_type",
                    "intercooperation",
                    "intercooperation_others",
                    "spaces",
                    "spaces_others",
                    "machinery",
                    "machinery_others",
                    "can_bring_machinery",
                    "new_people_criteria",
                ),
                "classes": ("tab-fs-specific",),  #'tab-fs-content', ),
                "description": "Informació específica sobre l'obrador. En cas de dubtes, podeu consultar l'apartat de <a target='_blank' href='/pagina/faqs/'>FAQS</a>",
            },
        ),
        (
            "Claus",
            {
                "fields": (
                    "titularitat_claus",
                    "other_categories",
                    "main_category",
                    "want_more_categories",
                    "category_comments",
                ),
                "classes": ("tab-fs-categories",),  #'tab-fs-content', ),
                #'description': 'Un altre text descriptiu'
            },
        ),
        (
            "Ubicació",
            {
                "fields": (
                    "address",
                    "location",
                ),
                "classes": ("tab-fs-location",),  #'tab-fs-content', ),
                #'description': 'Un altre text descriptiu'
            },
        ),
        (
            "Administració",
            {
                "fields": (
                    "created_date",
                    "modified_date",
                    "status",
                ),
                "classes": ("tab-fs-admin",),
                #'description': 'Un altre altre text descriptiu'
            },
        ),
        (
            "Elaboradores",
            {
                "fields": ("total_members",),
                "classes": ("tab-fs-members",),
                #'description': 'Un altre altre text descriptiu'
            },
        ),
    )
    baton_form_includes = [
        (
            "admin/evaluation.html",
            "editor_consent_date",
            "below",
        ),
    ]
    # def save_related(self, request, form, formsets, change):
    #     super(MapPointAdmin, self).save_related(request, form, formsets, change)
    # formset.save(commit=False) #This way, the inlines will not be saved to the database each time the main object is saved via the admin.
    # form.save_m2m()
    # obj = form.instance

    # for inlines in formsets:
    #     for inline_form in inlines:
    #         if inline_form.__class__.__name__ == "CommentForm":
    #             if inline_form.cleaned_data.get('id') is None:
    #                 text = inline_form.cleaned_data.get('text')
    #                 if text:
    #                     #saving new comment
    #                     mappoint = inline_form.cleaned_data.get('mappoint')
    #                     c = Comment.objects.create(text=text, mappoint=mappoint, author=request.user.userprofile)
    #                     inline_form.cleaned_data['id'] = c

    def get_search_results(self, request, queryset, search_term):
        return limit_list_to_owned_items(
            self, ObradorCompartitPointAdmin, request, queryset, search_term
        )

    #
    # def get_form(self, request, obj=None, **kwargs):
    #     form = super().get_form(request, obj, **kwargs)
    #     if not request.user.is_superuser:
    #         del form.base_fields
    #
    #     return form

    class Media:
        js = ("js/osm_map.js",)

    def save_model(self, request, obj, form, change):

        if not change:
            obj.editor = request.user
            obj.slug = slugify(obj.name)
        super().save_model(request, obj, form, change)
        form.save_criterias_form(request)

    def get_readonly_fields(self, request, obj=None):
        dynamic_fields = []
        if not request.user.is_superuser:
            dynamic_fields = ["status"]

        return list(self.readonly_fields) + dynamic_fields

    change_form_template = "admin/mappoint_changeform.html"

    def response_change(self, request, obj):
        onchange_review_buttons(self, request, obj, "obradorcompartitpoint")
        return super().response_change(request, obj)

    # Excluding fields for normal users
    def get_form(self, request, obj=None, **kwargs):
        if request.user.is_superuser:
            kwargs["fields"] = flatten_fieldsets(self.fieldsets)
            kwargs["fields"] += ["slug", "editor"]

        return super().get_form(request, obj, **kwargs)

    def get_fieldsets(self, request, obj=None):
        fieldsets = super().get_fieldsets(request, obj)
        newfieldsets = list(fieldsets)
        if request.user.is_superuser:
            fields = ["slug", "editor"]
            newfieldsets.append(["Camps per administradors", {"fields": fields}])
        return newfieldsets


from baton.autodiscover.admin import BatonAdminSite

BatonAdminSite.site_url = "https://obradorscompartits.cat"
site = BatonAdminSite()  # noqa
