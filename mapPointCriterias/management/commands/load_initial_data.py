from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.contrib.gis.geos import Point
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.utils.text import slugify
from taggit.models import TaggedItemBase

from mapPointCriterias.models import (
    Category,
    Criteria,
    PointStatusChoices,
    MapPointBasic,
    ObradorCompartitPoint,
)
import mapPointCriterias.initial_data as initial_data


def get_territories():
    # Get each country levels from https://wiki.openstreetmap.org/wiki/Tag:boundary%3Dadministrative
    # Get the overpass request: http://overpass-turbo.eu/s/10KE
    # [out:json][timeout:25];
    # {{geocodeArea:El Valle de Cofrentes-Ayora}}->.searchArea;
    # // gather results
    # (
    #   // query part for: “admin_level=10”
    #   node["admin_level"="8"](area.searchArea);
    #   way["admin_level"="8"](area.searchArea);
    #   relation["admin_level"="8"](area.searchArea);
    # );
    # // print results
    # out meta;
    # >;
    # out meta;

    return {}


class Command(BaseCommand):
    help = (
        "Generate fake data for local development and testing purposes"
        "and load it into the database"
    )

    def add_arguments(self, parser):
        parser.add_argument(
            "--delete",
            action="store_true",
            dest="delete",
            help="Delete previous highlights, homepage, landing page, milestones, news and people from the database",
        )

        parser.add_argument(
            "--seed",
            action="store",
            dest="seed",
            help="A seed value to pass to Faker before generating data",
        )

    def handle(self, *args, **options):

        if options["delete"]:
            call_command("flush_models")

        print("Flush all models")
        User.objects.all().delete()
        ObradorCompartitPoint.objects.all().delete()
        Group.objects.all().delete()
        Category.objects.all().delete()
        Criteria.objects.all().delete()
        TaggedItemBase.tag_model().objects.all().delete()

        admin = User.objects.create(
            username="kapis", is_staff=True, is_superuser=True, email="kapis@riseup.net"
        )
        admin.set_password("7yeonyie")
        admin.save()

        admin = User.objects.create(
            username="quim",
            is_staff=True,
            is_superuser=True,
            email="quim@trescadires.cat",
        )
        admin.set_password("8yeonyie")
        admin.save()

        test_user = User.objects.create(
            username="test", is_staff=True, is_superuser=False
        )
        test_user.set_password("test")
        test_user.save()

        test_xemac = User.objects.create(
            username="xemac", is_staff=True, is_superuser=False
        )
        test_xemac.set_password("xemac")
        test_xemac.save()

        users_group_xemac = Group.objects.create(name="EditoresXEMAC")
        models = ["espaitestagraripoint", "entitatsuportpoint"]
        for m in models:
            content_type = ContentType.objects.get(
                app_label="mapPointCriterias", model=m
            )
            permission = Permission.objects.get(
                codename=f"add_{m}", content_type=content_type
            )
            users_group_xemac.permissions.add(permission)
            permission = Permission.objects.get(
                codename=f"view_{m}", content_type=content_type
            )
            users_group_xemac.permissions.add(permission)
            permission = Permission.objects.get(
                codename=f"change_{m}", content_type=content_type
            )
            users_group_xemac.permissions.add(permission)
            permission = Permission.objects.get(
                codename=f"delete_{m}", content_type=content_type
            )
            users_group_xemac.permissions.add(permission)

        users_group = Group.objects.create(name="EditoresObradors")
        models = ["obradorcompartitpoint", "pointimage", "pointmember"]
        for m in models:
            content_type = ContentType.objects.get(
                app_label="mapPointCriterias", model=m
            )
            permission = Permission.objects.get(
                codename=f"add_{m}", content_type=content_type
            )
            users_group.permissions.add(permission)
            permission = Permission.objects.get(
                codename=f"view_{m}", content_type=content_type
            )
            users_group.permissions.add(permission)
            permission = Permission.objects.get(
                codename=f"change_{m}", content_type=content_type
            )
            users_group.permissions.add(permission)
            permission = Permission.objects.get(
                codename=f"delete_{m}", content_type=content_type
            )
            users_group.permissions.add(permission)

        test_user.groups.add(users_group)
        test_user.groups.add(users_group_xemac)
        test_xemac.groups.add(users_group_xemac)

        for order_num, data in enumerate(initial_data.CATEGORIES):
            slug, cat = data
            Category.objects.create(
                name=cat,
                my_order=order_num,
                parent=None,
                slug=slug,
                icon=f"category_icons/{slug}.png",
            )

        # for status in initial_data.STATUS:
        #     PointStatus.objects.create(codename=status["codename"], name=status["name"], type=status["type"])

        for order_num, data in enumerate(initial_data.SIMPLE_CRITERIAS):
            Criteria.objects.create(
                name=data["title"],
                slug=slugify(data["title"]),
                icon=data["icona"],
                description=data["description"],
                question=data["question"],
                my_order=order_num,
            )

        from faker import Faker
        import random

        fake = Faker()
        for num in range(0):
            under_construction = bool(random.getrandbits(1))
            is_blanc = bool(random.getrandbits(1))
            if under_construction:
                status = [
                    PointStatusChoices.PUBLISHED,
                    PointStatusChoices.DRAFT,
                    PointStatusChoices.REVIEW_PENDING,
                    PointStatusChoices.ARCHIVED,
                ][int(random.getrandbits(2))]
            else:
                status = PointStatusChoices.PUBLISHED
            main_cat = Category.objects.order_by("?").first()

            name = f"{num}. Obrador {fake.name()}"
            if under_construction:
                name += " (en construcció)"
            print(num, name, status)
            intro = fake.sentence()
            # body = fake.text()
            picture_path = f"point_images/{num:03}.jpg"
            p = ObradorCompartitPoint.objects.create(
                name=name,
                slug=slugify(name),
                status=status,
                picture=picture_path,
                phone=fake.phone_number(),
                intro=intro,
                main_category=main_cat,
                under_construction=under_construction,
                web=fake.url(),
                is_blanc=is_blanc,
                admin_email=fake.email(),
                address=fake.address(),
                editor=test_user,
            )

            for i in range(random.randint(0, 10)):
                tag = f"#etiq{i+1}"
                p.tags.add(tag)
                cat = Category.objects.order_by("?").first()
                if not cat in p.other_categories.all():
                    p.other_categories.add(cat)

            lng = random.uniform(0.8082, 2.1376)
            lat = random.uniform(41.3477, 42.3461)
            p.location = Point(lng, lat, srid=4326)

            p.save()

        print(self.style.SUCCESS("Done!"))
