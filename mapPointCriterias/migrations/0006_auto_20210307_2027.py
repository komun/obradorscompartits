# Generated by Django 3.0 on 2021-03-07 19:27

from django.db import migrations, models
import mapPointCriterias.models


class Migration(migrations.Migration):

    dependencies = [
        ('mapPointCriterias', '0005_page'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='content',
            field=mapPointCriterias.models.CharFieldWithBigTextarea(help_text='Compatible amb jquery 3.5, FontAwesome 5.2 i Bootstrap 4.5', verbose_name='Contingut HTML'),
        ),
        migrations.AlterField(
            model_name='page',
            name='url',
            field=models.CharField(help_text='Es trovarà com a https://obradorscompartits.cat/pagina/[URL]', max_length=50, verbose_name='URL'),
        ),
    ]
