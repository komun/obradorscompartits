from django.core.mail import EmailMessage
from django.conf import settings


def send_admin_notification(template, point_type, point):
    subject = f"Nou {point_type} en estat de revisió"
    body = f"Visita {settings.SITE_URL}/admin/mapPointCriterias/{point_type}/?status__exact=REVIEW_PENDING per aprovar els punts en espera."
    print("EMAILED ADMIN NOTIFICATION", subject, body)
    email = EmailMessage(
        subject, body, settings.DEFAULT_FROM_EMAIL, settings.ADMINS_EMAIL
    )
    email.send(fail_silently=False)
    return True


def send_user_notification(template, point_type, point):
    if template == "_make-disapprove":
        subject = f"El punt '{point.name}' NO ha sigut aprovat"
        if point.editor_email:
            editor_email = point.editor_email
        else:
            editor_email = point.editor.email
        body = f"El teu punt ha tornat a l'estat de borrador després de la revisió. Pots millorar el contingut a: {settings.SITE_URL}/admin/"
    elif template == "_make-approve":
        subject = f"El punt '{point.name}' ha sigut aprovat"
        if point.editor_email:
            editor_email = point.editor_email
        else:
            editor_email = point.editor.email
        body = f"El teu punt ha sigut aprovat després de la revisió i ja es pot veure a {settings.SITE_URL}/?id={point.slug}"
    print("EMAILED USER NOTIFICATION", subject, body, "TO: ", editor_email)
    email = EmailMessage(
        subject,
        body,
        settings.DEFAULT_FROM_EMAIL,
        [editor_email],
        settings.ADMINS_EMAIL,
        reply_to=[settings.DEFAULT_REPLYTO_EMAIL],
    )
    email.send(fail_silently=False)
    return True
