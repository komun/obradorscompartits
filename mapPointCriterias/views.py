import os.path
from itertools import chain
from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.contrib.auth.models import Group
from .models import (
    Category,
    ObradorCompartitPoint,
    EspaiTestAgrariPoint,
    EntitatSuportPoint,
    PointStatusChoices,
    ChoiceArrayField,
    Page,
)
from django.db.models import fields as djangofields
from django_registration.backends.one_step.views import (
    RegistrationView as BaseRegistrationView,
)


COLORS = {
    "disabled": "#808080",
    "agroecologic": "#800080",
    "default": "#000000",
}


def get_feature(lon, lat):
    return {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [
                float(lon),
                float(lat),
            ],
        },
        "properties": {},
    }


# Create your views here.
# from django.views.generic.edit import CreateView
def frontend_view(request):
    context = {}
    return render(request, "frontend/map_listing_index_page.html", context)


category_xtest = {
    "name": "Espais Test Agraris",
    "icon_name": "/category_icons/xemac_espais_test_agraris.png",
    "icon_marker_color": COLORS["default"],
    "slug": "xtest",
}
category_xsuport = {
    "name": "Entitats de Suport",
    "icon_name": "/category_icons/xemac_servei_assesorament.png",
    "icon_marker_color": COLORS["default"],
    "slug": "xsuport",
}


def get_categories(request, language):
    qs = Category.objects.filter(parent_id__isnull=True).order_by("my_order")
    items = list(qs.values())
    categories = {}
    sorted_slugs = []
    sorted_slugs.append("agroecologic")
    categories["agroecologic"] = {
        "name": "Agroecològics",
        "icon_name": "/category_icons/empty.png",
        "icon_marker_color": COLORS["agroecologic"],
        "slug": "agroecologic",
    }

    # sorted_slugs.append("social")
    # categories["social"] = {
    #     "name": "De capital social",
    #     "icon_name": "/category_icons/empty.png",
    #     "icon_marker_color": COLORS["social"],
    #     "slug": "ecologic",
    # }

    categories["obrador_pendent"] = {
        "name": "Obradors compartits en construcció",
        "icon_name": "/category_icons/obrador_pendent.png",
        "icon_marker_color": COLORS["disabled"],
        "icon_disabled": True,
        "slug": "obrador_pendent",
    }
    sorted_slugs.append("obrador_pendent")
    for item in items:
        slug = item["slug"]
        name = item["name"]
        icon = item["icon"]
        icon_marker_color = COLORS["default"]
        categories[slug] = {
            "slug": slug,
            "name": name,
            "icon_name": icon,
            "icon_marker_color": icon_marker_color,
        }
        sorted_slugs.append(slug)

    sorted_slugs.append("xtest")
    categories["xtest"] = category_xtest

    sorted_slugs.append("xsuport")
    categories["xsuport"] = category_xsuport

    output = {"sorted_slugs": sorted_slugs, "categories": categories}

    return JsonResponse(output, safe=False)


def getMapPoints(request, language):
    json_points = []
    obradors_points = ObradorCompartitPoint.objects.filter(
        status=PointStatusChoices.PUBLISHED, location__isnull=False
    )
    xtest_points = EspaiTestAgrariPoint.objects.filter(
        status=PointStatusChoices.PUBLISHED, location__isnull=False
    )
    xsuport_points = EntitatSuportPoint.objects.filter(
        status=PointStatusChoices.PUBLISHED, location__isnull=False
    )
    points = chain(obradors_points, xtest_points, xsuport_points)

    for point in points:
        json_p = get_feature(lon=point.location.coords[0], lat=point.location.coords[1])

        json_p["properties"]["name"] = (
            f"{point.name} (en construcció)" if point.under_construction else point.name
        )
        json_p["properties"]["filter_name"] = point.name
        json_p["properties"]["slug"] = point.slug
        json_p["properties"]["description"] = point.intro
        json_p["properties"]["icon_marker_color"] = COLORS["default"]

        # json_p['properties']['icon_name'] = 'question'
        # json_p['properties']['icon_marker_color'] = 'red'
        # json_p['properties']['icon_prefix'] = 'fa'
        if point.under_construction:
            json_p["properties"]["icon_disabled"] = True
            json_p["properties"]["icon_marker_color"] = COLORS["disabled"]

        if type(point) == ObradorCompartitPoint:
            json_p["properties"]["icon_name"] = point.main_category.icon.name
            json_p["properties"]["categories"] = point.get_slug_categories()
            if point.is_ecologic:
                json_p["properties"]["icon_marker_color"] = COLORS["agroecologic"]

        elif type(point) == EspaiTestAgrariPoint:
            json_p["properties"]["icon_name"] = category_xtest["icon_name"]
            json_p["properties"]["categories"] = [category_xtest["slug"]]
        elif type(point) == EntitatSuportPoint:
            json_p["properties"]["icon_name"] = category_xsuport["icon_name"]
            json_p["properties"]["categories"] = [category_xsuport["slug"]]

        json_p["properties"]["slug"] = point.slug
        # json_p["properties"]["tags"] = list(
        #     point.tags.all().values_list("name", flat=True)
        # )
        json_points.append(json_p)

    response_json = {"response": json_points, "status": 200}
    return JsonResponse(response_json, safe=False)


def _get_frontend_text_from_field_verbose(point, field_name):
    match_phrases = {"admin_name": "Entitat/empresa gestionadora"}
    if field_name in match_phrases:
        return match_phrases[field_name]

    else:
        field = getattr(point.__class__, field_name).field
        if type(field) == ChoiceArrayField:
            return field.formfield().label
        else:

            return field.verbose_name


def _get_frontend_value_from_field(point, field_name):
    field_value = getattr(point, field_name)

    field = getattr(point.__class__, field_name).field
    return_value = field_value
    if type(field) == djangofields.CharField:
        if field.choices:
            for c, v in field.choices:
                if c == field_value:
                    return_value = v
        else:
            return_value = field_value
    if type(field_value) == list:

        choices = field.formfield().choices
        if len(field_value):
            return_value = "<ul style='margin:0px'>"
            for val in field_value:
                for c, v in choices:
                    if c == val:
                        return_value += f"<li>{v}</li>"
            return_value += "</ul>"
        else:
            return_value = "--"

    return return_value


def _generateObradorBody(point):
    body = ""

    fields = [
        "obrador_type",
        "is_kickstarter",
        "is_agroecologic",
        "ecologic_certified",
        "is_blanc",
        "management_type",
        "participation_type",
        "intercooperation",
        "intercooperation_others",
        "spaces",
        "spaces_others",
        "machinery",
        "machinery_others",
        "can_bring_machinery",
        "new_people_criteria",
        "titularitat_claus",
        "want_more_categories",
        "category_comments",
        "total_members",
    ]

    for field in fields:
        verbose_name = _get_frontend_text_from_field_verbose(point, field)
        value_name = _get_frontend_value_from_field(point, field)
        if type(value_name) == bool:
            value_name = "SÍ" if value_name else "NO"
        if not value_name == "" and not value_name == "--" and value_name is not None:
            if verbose_name.endswith("?") or verbose_name.endswith("."):
                body += f"<br><br><b>{verbose_name}</b> {value_name}"
            else:
                body += f"<br><br><b>{verbose_name}</b>: {value_name}"

    return body


def showMapPoint(request, language, slug):
    if slug.startswith("xtest_"):
        point = EspaiTestAgrariPoint.objects.filter(slug=slug).first()
        cat_dict = {
            category_xtest["slug"]: {
                "name": category_xtest["name"],
                "icon_name": category_xtest["icon_name"],
            }
        }

    elif slug.startswith("xsuport_"):
        point = EntitatSuportPoint.objects.filter(slug=slug).first()
        cat_dict = {
            category_xsuport["slug"]: {
                "name": category_xsuport["name"],
                "icon_name": category_xsuport["icon_name"],
            }
        }

    if slug.startswith("xtest_") or slug.startswith("xsuport_"):
        tags = point.tags
        picture_url = point.picture.name if point.picture else None
        email_parts = []
        if point.admin_email and not point.admin_email.startswith("email@"):
            email_parts = point.admin_email.split("@")

        intro = point.intro
        title = point.name
        tags = point.tags

        html = render(
            request,
            "frontend/map_simple_card.html",
            {
                "point": point,
                "picture_url": picture_url,
                "title": title,
                "email_parts": email_parts,
                "categories": cat_dict,
                "lon": str(point.location.coords[0]).replace(",", "."),
                "lat": str(point.location.coords[1]).replace(",", "."),
                "tags": tags.all().values_list("name", flat=True),
            },
        )

    else:
        point = ObradorCompartitPoint.objects.filter(slug=slug).first()
        if not point:
            html = "Item not found"
        else:
            body = _generateObradorBody(point)
            gallery_images = []

            if point.point_images:
                for img in point.point_images.all():
                    gallery_images.append(
                        {
                            "thumb_url": img.picture.url,  # .get_rendition('fill-200x200-c100').url,
                            "caption": "",  # img.caption,
                            "original_url": img.picture.url,
                        }
                    )
            picture_url = point.picture.name if point.picture else None
            cat_dict = {}
            for cat in point.get_other_categories_slugs():
                cat_dict[cat.slug] = {"name": cat.name, "icon_name": cat.icon.name}

            intro = point.intro
            title = point.name
            tags = point.tags
            # body = point.body

            # url = get_url_from_point(point, lang=t)

            email_parts = []
            if point.admin_email and not point.admin_email.startswith("email@"):
                email_parts = point.admin_email.split("@")

            html = render(
                request,
                "frontend/map_right_card.html",
                {
                    "intro": intro,
                    "title": title,
                    "body": body,
                    "point": point,
                    "gallery_images": gallery_images,
                    "picture_url": picture_url,
                    "categories": cat_dict,
                    "email_parts": email_parts,
                    "lon": str(point.location.coords[0]).replace(",", "."),
                    "lat": str(point.location.coords[1]).replace(",", "."),
                    "tags": tags.all().values_list("name", flat=True),
                },
            )

    return HttpResponse(html, content_type="text/html")


@csrf_exempt
def api_endpoints(request, endpoint):
    print("API Endpoint called: ", endpoint)
    language = request.GET.get("lang", "")
    slug = request.GET.get("slug", "")

    if endpoint == "getCategories":
        response = get_categories(request, language)
    elif endpoint == "getMapPoints":
        response = getMapPoints(request, language)
    elif endpoint == "showMapPoint":
        response = showMapPoint(request, language, slug)
    else:
        return HttpResponse("Pàgina no trovada.")
    return response


@csrf_exempt
def page_endpoints(request, pagename):
    # TEMPLATE_DIR = os.path.join(settings.BASE_DIR, "templates")
    context = {}
    page = Page.objects.filter(url=pagename)
    if not page:
        return HttpResponse("Pàgina no trovada.")
    else:
        template_name = "frontend/basepage.html"
        context["content"] = page.first().content
        return render(request, template_name, context)


class RegistrationView(BaseRegistrationView):
    """
    Registration overwritten to allow django admin access to specific group

    """

    def register(self, form):
        new_user = super().register(form)
        group_obradors = Group.objects.get(name="EditoresObradors")
        # group_xemac = Group.objects.get(name="EditoresXEMAC")
        new_user.groups.add(group_obradors)
        # new_user.groups.add(group_xemac)
        new_user.is_staff = True
        new_user.save()
        return new_user
