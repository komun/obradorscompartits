from django.apps import AppConfig


class MappointCriteriasConfig(AppConfig):
    name = 'mapPointCriterias'
    verbose_name = "Gestió dels punts"
