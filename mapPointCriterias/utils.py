from .models import (
    PointStatusChoices,
)
from .notifications import send_admin_notification, send_user_notification


def limit_list_to_owned_items(self, myclass, request, queryset, search_term):

    queryset, use_distinct = super(myclass, self).get_search_results(
        request, queryset, search_term
    )

    if not request.user.is_superuser:  # TODO si son del grupo de admin
        queryset = queryset.filter(editor=request.user)
    return queryset, use_distinct


def onchange_review_buttons(self, request, obj, point_type="obradorcompartitpoint"):
    if "_make-reviewing" in request.POST:
        obj.status = PointStatusChoices.REVIEW_PENDING
        obj.save()
        # Send notification to point type admins
        send_admin_notification(
            template="_make-reviewing", point_type=point_type, point=obj
        )
        self.message_user(
            request,
            "El contingut de l'obrador serà revisat per l'equip motor abans de ser publicat.",
        )
    elif "_make-approve" in request.POST:
        obj.status = PointStatusChoices.PUBLISHED
        obj.save()
        send_user_notification(
            template="_make-approve", point_type=point_type, point=obj
        )
        self.message_user(
            request,
            "El punt ha sigut aprovat i la/el responsable rebrà una notificació.",
        )
    elif "_make-disapprove" in request.POST:
        obj.status = PointStatusChoices.DRAFT
        obj.save()
        send_user_notification(
            template="_make-disapprove", point_type=point_type, point=obj
        )
        self.message_user(
            request,
            "El punt ha tornat a l'estat d'esborrany. La/el responsable rebrà una notificació.",
        )
