from django import template
from mapPointCriterias.models import Page

register = template.Library()


@register.simple_tag
def get_index_html():
    index_html = Page.objects.filter(url="index")
    if index_html:
        return index_html.first().content
    else:
        return ""
