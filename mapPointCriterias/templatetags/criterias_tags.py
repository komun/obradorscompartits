from django import template
from django.core.exceptions import ObjectDoesNotExist

from mapPointCriterias.models import Criteria, EvaluationCriteriasForMapPoint
from mapPointCriterias.forms import ObradorCompartitPointForm

register = template.Library()


@register.simple_tag(takes_context=True)
def get_all_criterias(context, queryset, **filters):

    return Criteria.objects.all().order_by("my_order")  # filter(**filters).first()


@register.simple_tag(takes_context=True)
def get_criteria_answers(context, point, criteria, **filters):

    form = ObradorCompartitPointForm(context.request.POST or None)
    if not context.request.POST or form.is_valid():
        try:
            answer = EvaluationCriteriasForMapPoint.objects.get(
                criteria=criteria, mappoint=point
            )
        except ObjectDoesNotExist:
            answer = None
    else:
        overal_points = context.request.POST[f"criteria_points-{criteria.my_order}"]
        if overal_points != "none":
            overal_points = float(overal_points)
        answer = {
            "overal_points": overal_points,
            "notes": context.request.POST[f"criteria_comment-{criteria.my_order}"],
        }

    # import ipdb
    #
    # ipdb.set_trace()
    return answer
