
var allitems = []; // array with all the items before filtering
var items = []; // the items showed in the map, after the categories
var categories = {};
var sorted_category_slugs = [];
var filters = [];
var allCheckbox = {checked: true};
var $jq = jQuery.noConflict();

function copyToClipboard(element) {
  var $temp = $jq("<input>");
  $("body").append($temp);
  $temp.val(element).select();
  document.execCommand("copy");
  $temp.remove();
  $('#alert_copied').show();
}

function hexToRGBA(hex,opacity){
    hex = hex.replace('#','');
    r = parseInt(hex.substring(0,2), 16);
    g = parseInt(hex.substring(2,4), 16);
    b = parseInt(hex.substring(4,6), 16);

    result = 'rgba('+r+','+g+','+b+','+opacity+')';
    return result;
}

function removeEmptyCategories(points, slug_categories){
  let new_categories = new Set();
  points.forEach(function(point){
    point.properties.categories.forEach(function(cat){ new_categories.add(cat) });
  });
  sorted_categories = [];
  slug_categories.forEach(function(cat){
    if (new_categories.has(cat) ){ sorted_categories.push(cat);} //categories.indexOf(cat), 1);
  });
  // debugger;
  return sorted_categories;
}


function insertPointsInFilters(points) {

   points.forEach(function(point){
     point.properties.categories.forEach(function(cat){

        input = $jq("[id='labelfilter-"+cat+"']");
        a_tag = '<a href="javascript:goToItem(\''+point.properties.slug+'\')">'
        point_html = input.html() + "<br><big>˪</big> "+a_tag+"<span class='point_in_filter '"+url+">"+point.properties.filter_name+"</span></a>"
        input.html(point_html)
        // debugger;
      });
   });

}

function printFiltersList() {
  var html = "";

  sorted_category_slugs.forEach(function(category){
    if (categories.hasOwnProperty(category)) {
      if(category.indexOf('.') !== -1) div_class = 'lvl2';
      else div_class = 'lvl1';
//      div_class = 'lvl1';
      if(categories[category].hasOwnProperty("icon_disabled")){
          var opacity_decimal = 0.5
          var opacity_integer = 50
      }else{
          var opacity_decimal = 1
          var opacity_integer = 100
      }

      html += '<div class="'+div_class+'"> \
                <label id="labelfilter-' + category + '"><input id="check-' + category + '" onclick="updateFilter(\''+category+'\')" type="checkbox" value="" checked="'+categories[category].checked+'"> \
                  <img style="width:30px;height:30px; opacity: '+opacity_decimal+'; filter: alpha(opacity='+opacity_integer+'); border: 2px solid '+categories[category].icon_marker_color+';" \
                  class="marker" src="/media/' + categories[category].icon_name + '" /> '+categories[category].name+'</input></label></div>';
    }
  });

  document.getElementById("filtersList").innerHTML = html;
}

function getItem(id) {
  for(var i=0; i<items.length; i++) {
    if(items[i].properties.slug==id) {
      return(items[i]);
    }
  }
  return("not found");
}


function showRightCard(slug) {
  $jq.get("/api/showMapPoint/?lang="+lang+"&slug="+slug, function(data){
    rightCard = data;
    $jq("#info_point").html(rightCard);
    sidebar.open('sidebar_info_point');
    $jq('.sidebar-content').scrollTop(0);
  });

  // document.getElementById("rightCard").className+=" rightCard-show";
}

function hideRightCard() {
  $jq("#info_point").html();
}

function turnLoadingBar(state){
  $jq("#loadingbar").toggle()
}
function updateMarkers(id_to_popup) {
  var list_markers = {};
  markers.clearLayers();
  for(var i=0; i<items.length; i++) {
    var lat = items[i].geometry.coordinates[1];
    var lon = items[i].geometry.coordinates[0];
    // console.log("lat "+lat+" lon "+lon);
    if(items[i].properties.hasOwnProperty("icon_disabled")){
        var opacity_decimal = 0.5
        var opacity_integer = 50
    }else{
        var opacity_decimal = 1
        var opacity_integer = 100
    }
    var icon_html = '<img style="opacity: '+opacity_decimal+'; filter: alpha(opacity='+opacity_integer+'); width:30px;height:30px;border: 2px solid '+items[i].properties.icon_marker_color+'; \
    box-shadow: 0 0 5px 4px ' + hexToRGBA(items[i].properties.icon_marker_color, 0.2) + ';" class="marker" src="/media/' + items[i].properties.icon_name + '" />'
    var icon = L.divIcon({
        className: 'markerInvisible',
        popupAnchor:  [10, 0], // point from which the popup should open relative to the iconAnchor
        html: icon_html
    });
    var marker = L.marker([lat, lon], {icon: icon});
    marker.id = items[i].properties.slug;
    if(items[i].properties.description!=null) {
      marker.bindPopup("<b>"+items[i].properties.name+"</b><br><i>"+items[i].properties.description+"</i>");
    } else {
      marker.bindPopup("<b>"+items[i].properties.name+"</b>");
    }
    marker.on('mouseover', function(e) {
      this.openPopup();
    });
    marker.on('mouseout', function(e) {
      this.closePopup();
    });
    marker.on('click', function(e) {
      slug = this.id
      var item = getItem(slug);
      var lat = item.geometry.coordinates[1];
      var lon = item.geometry.coordinates[0];
      var zoom = 8;
      showRightCard(slug);
      window.history.pushState({},"", "?id=" + slug);
    });
    // console.log(marker)
    markers.addLayer(marker);
    list_markers[marker.id] = marker;
    if(id_to_popup != undefined && marker.id == id_to_popup){
        showInMap(items[i]);
    }

  }

  map.addLayer(markers);

  if(id_to_popup != undefined){
      if(list_markers.hasOwnProperty(id_to_popup)){
          m = list_markers[id_to_popup];

          markers.zoomToShowLayer(m, function() {
              if (!m._icon) m.__parent.spiderfy();
              m.openPopup();
          });
      }
  }

  return list_markers

}
function updateFilter(selectedFilter) {
  turnLoadingBar(true);
  category = categories[selectedFilter];
  if(category.checked == undefined || category.checked===true) {
    category.checked = false;
    document.getElementById('check-' + selectedFilter).checked = false;
  } else {
    category.checked = true;
    document.getElementById('check-' + selectedFilter).checked = true;
  }
  if(selectedFilter.indexOf('.') === -1){ selectChildrenCheckboxes(selectedFilter, category.checked)}
  setTimeout(function() {
        applyFilters();
        turnLoadingBar(false);
    }, 0);

}

function clickAllFilters(){
    turnLoadingBar(true);
    setTimeout(function() {
        selectAllFilters();
        turnLoadingBar(false);
    }, 0);

}
function selectAllFilters(empty) {
    if(empty !== undefined || allCheckbox.checked===true) {
      items = [];
      allCheckbox.checked = false;
      document.getElementById('allCheckbox').checked = false;
      for (var property in categories) {
          categories[property].checked=false;
          document.getElementById('check-' + property).checked = false;
      }
    } else {
      items = JSON.parse(JSON.stringify(allitems));
      allCheckbox.checked= true;

      for (var property in categories) {
          categories[property].checked=true;
          document.getElementById('check-' + property).checked = true;
        }
      document.getElementById('allCheckbox').checked = true;
    }
    list_markers = updateMarkers();
}

function itemInItems(array, el) {
  for(var i=0;i<array.length; i++) {
    if(array[i].properties.slug===el.properties.slug) {
      return true;
    }
  }
  return false;
}

function selectChildrenCheckboxes(filter, status){
    for (var property in categories) {
        if(property.indexOf(filter) !== -1){ //if is a subcheckbox contains the name of the parent category
            if (categories[property].checked == undefined || categories[property].checked!==status) {
                categories[property].checked = status;
                document.getElementById('check-' + property).checked = status;
            }
        }
    }
}

function applyFilters() {
  items = [];

  for(var i=0; i<allitems.length; i++) {
      allitems[i].properties.categories.forEach(function (property) {
            if(categories[property].checked == undefined || categories[property].checked===true) {
              // check if item is not yet in items array
              if(itemInItems(items, allitems[i])===false) {
                items.push(JSON.parse(JSON.stringify(allitems[i])));
              }
            }
        });
      }
  list_markers = updateMarkers();
}

function updateLanguage(lang) {
  document.getElementById("lang").value = lang;
}
function printSearchResults(items) {
  var html = "";
  html += "<div class='list-group'>";
  for(var i=0; i<items.length; i++) {
    html += '<div onclick="goToItem(\'' + items[i].properties.slug + '\')" class="list-group-item list-group-item-action flex-column align-items-start"> \
        <div class="d-flex w-100 justify-content-between"> \
          <h5 class="mb-1">' + items[i].properties.name + '</h5> \
          <small>3 days ago</small> \
        </div>';
      if(items[i].properties.description!=null) {
        html += '<p class="mb-1">' + items[i].properties.description + '</p>';
      }
      html += '</div>';
  }
  html += "</div>";
  document.getElementById("searchResults").innerHTML = html;
}
function search() {
  var searchResults = [];
  var key = document.getElementById("searchinput").value.toUpperCase();
  if(key=="") {
    document.getElementById("searchResults").innerHTML = "";
    return;
  }
  for(var i=0; i<allitems.length; i++) {
    if(allitems[i].properties.name.toUpperCase().includes(key)) {
      searchResults.push(allitems[i]);
    }
  }
  printSearchResults(searchResults);
}
function showInMap(item) {
  lat = item.geometry.coordinates[1];
  lon = item.geometry.coordinates[0];
//  window.history.pushState({},"", "?l="+lang+"&lat=" + lat + "&lon=" + lon + "&zoom=" + zoom);
  map.setView([lat, lon], 16);
  window.history.pushState({},"", "?id=" + item.properties.slug);
}
function showPopup(slug){
    if(list_markers.hasOwnProperty(slug)){
      m = list_markers[slug];

      markers.zoomToShowLayer(m, function() {
          if (!m._icon) m.__parent.spiderfy();
          m.openPopup();
      });
  }
}
function goToItem(id) {
  var item = getItem(id);
  showRightCard(id);
  showInMap(item);
  showPopup(id);

}

function checkInitialFilters(param_filters){
    // if(param_filters == 'monedasocial' || param_filters == 'socialcurrency' || param_filters == 'sel'){
    //     param_filters = ['socialcurrency', 'socialcurrency.inactive_network', 'socialcurrency.active_network'];
    // }
    if (param_filters){
        turnLoadingBar(true);
        if( typeof param_filters === 'string' ) {
            param_filters = [ param_filters ];
        }
        setTimeout(function() {
            selectAllFilters(false);
            for (var i in param_filters) {
                filter = param_filters[i];
                if (categories.hasOwnProperty(filter)) {
                    document.getElementById('check-' + filter).checked = true;
                    categories[filter].checked = true;
                }
            }

            applyFilters();
            turnLoadingBar(false);
        }, 0);


    }

}

$jq.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}

// -----
// init
// -----

var url = window.location.href
var lang = $jq.urlParam("lang");
if(lang==undefined) {
  lang = $jq.urlParam("l");
}
var lat = $jq.urlParam("lat");
var lon = $jq.urlParam("lon");
var zoom = $jq.urlParam("zoom");
var placeid = $jq.urlParam("placeid");
var param_filters = $jq.urlParam("filters");
if(placeid==undefined){
    placeid = $jq.urlParam("id");
}


if((lat==undefined)||(lon==undefined)) {
  lat = 41.76517;
  lon = 1.91711;
}
if(zoom==undefined) {
  zoom=8;
}
if(lang==undefined) {
  if (language!=undefined){
        lang = language;
        window.history.pushState({},"", "?l="+lang+"&lat=" + lat + "&lon=" + lon + "&zoom=" + zoom);
  }else{
        lang = "en";
  }
}


tiles_url = 'https://a.tile.openstreetmap.org/{z}/{x}/{y}.png'

var tiles = L.tileLayer(tiles_url, {
    maxZoom: 18,
    attribution: 'Desenvolupat per <a target="_blank" href="https://komun.org/ca">Komun.org</a> Mapa: &copy; OpenStreetMap'
});

var map = L.map('map', {layers: [tiles]});
map.setView([lat, lon], zoom);
// alert(lat+" "+lon+" "+zoom+" ")
map.addEventListener('dragend', function(ev) {
  var coord = map.getCenter();
   lat = Math.round(coord.lat * 100000) / 100000;
   lon = Math.round(coord.lng * 100000) / 100000;
   window.history.pushState({},"", "?l="+lang+"&lat=" + lat + "&lon=" + lon + "&zoom=" + zoom);
});
// map._mapPane.style.zIndex = -2;
map.on('zoomend', function(ev) {
    zoom = ev.target._zoom;
    window.history.pushState({},"", "?l="+lang+"&lat=" + lat + "&lon=" + lon + "&zoom=" + zoom);
});
var markers = L.markerClusterGroup({
    maxClusterRadius: function (zoom) {
        return (zoom <= 14) ? 40 : 1; // radius in pixels
    },
    // maxClusterRadius: 40,
    animateAddingMarkers: true
});
var list_markers = [];
if (placeid !== null && placeid != undefined && placeid !== "null" && placeid !== "none") {
  showRightCard(placeid);
  var id_to_popup = placeid;
}

lang = 'ca';
$jq.get("/api/getCategories/?lang="+lang, function(data){
  backend_categories = data["categories"];
  sorted_category_slugs = data["sorted_slugs"];

  var social_currency_networks = [];
  var database_points = [];
  // get items data

  $jq.get("/api/getMapPoints/?lang="+lang+"", function(data){
    allitems = JSON.parse(JSON.stringify(data.response));
    items = allitems;
    if(param_filters == undefined){
        list_markers = updateMarkers(id_to_popup);
        turnLoadingBar(false);
    }
    sorted_category_slugs = removeEmptyCategories(allitems, sorted_category_slugs);
    categories = {};
    sorted_category_slugs.forEach(function(cat){
          categories[cat] = backend_categories[cat]
    });

    printFiltersList();
    insertPointsInFilters(allitems);

    // //Unchecking last categories manually
    // filter_xemac = document.getElementById('check-xsuport')
    // // categories["xsuport"].checked = false;
    // if(typeof  filter_xemac !== 'undefined'){ document.getElementById('check-xsuport').checked = false;}
    // filter_xemac = document.getElementById('check-xtest')
    // categories["xtest"].checked = false;
    // if(typeof  filter_xemac !== 'undefined'){ document.getElementById('check-xtest').checked = false;}
    checkInitialFilters(param_filters);
    applyFilters();
  });

});

function detectmob() {
  if(window.innerWidth <= 800 && window.innerHeight <= 600) {
    return true;
  } else {
    return false;
  }
}

var sidebar = L.control.sidebar('sidebar').addTo(map);
if(!detectmob()) {
  sidebar.open('sidebar_home');
  // sidebar.open('sidebar_filter');
  // if(lat == default_lat && lon == default_lon){
  //   lon = -1;
  //   map.setView([lat, lon], zoom);
  // }
}
