var $jq = jQuery.noConflict();
var mymap = null;

$jq(window).on('map:init', function(e) {
  mymap = e.originalEvent.detail.map;
});

$jq(document).ready(function(e) {
  var checkExist = setInterval(function() {

    if ($jq('.leaflet-draw-draw-marker').length) {
      console.log("Exists!");

      location_textarea = $jq('#id_location')[0];
      location_textarea.cols = 60;
      location_textarea.rows = 1;
      // By default, we enable the draw marker
      $jq('.leaflet-draw-draw-marker')[0].click();
      // Hide the move/edit button as it's not very usable
      $jq('.leaflet-draw-edit-edit')[0].hidden = true;

      clearInterval(checkExist);
    }
  }, 100);

  // Only call invalidateSize if mymap is defined
  setInterval(function() {
    if (mymap !== null) {
      mymap.invalidateSize();
    } else {
      console.warn("mymap is not yet initialized.");
    }
  }, 6000);
});
