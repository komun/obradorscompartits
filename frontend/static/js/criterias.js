var $jq = jQuery.noConflict();


function display_circle_points(criteria_id, points) {

   if(points == null){points = 0;}
   if(points > 5){points = 5;}
   non_decimal = Math.floor(points)
   decimals = points - non_decimal
   if(decimals > 0){ half_point = 1;}
   else{half_point = 0;}
   circles_html = '<img width=14 height=14  src="/static/img/circle.png">'.repeat(non_decimal)
   circles_html = circles_html + '<img height=14 src="/static/img/circle_half.png">'.repeat(half_point)
   if (points == 0){ circles_html = '<img width=14 height=14 src="/static/img/circle_empty.png">'}
   circle_label = $jq("[id='circles-"+criteria_id+"']")
   circle_label.html(circles_html)
}

function dont_apply(obj) {
  // debugger;
  $n = obj.attr('id').split("-").pop();
  $answers = $jq("[id='answers-"+$n+"']");
  $criteria_points = $jq("[id='criteria_points-"+$n+"']");
  $circles = $jq("[id='circles-"+$n+"']");

  if (obj.is(":checked")) {
      //$question.toggleClass('strike');
      $answers.hide()
      $criteria_points.hide()
      $circles.hide()
  } else {
     //$question.toggleClass('strike');
     $answers.show();
     $circles.show()
     if($jq("[name='evaluation_result_type']").val() == "OVERALL"){
        $criteria_points.show()
     }
  }
}

function refresh_circles(){
  for (let i =0; i <= 7; i++) {
    if($jq("[name='evaluation_result_type']").val() == "OVERALL"){
       value = $jq("[name='criteria_points-"+i+"']").val()
       if(value == "none"){value = 0}
       else{ value = parseFloat(value) }
    }else{ //Sum answers result
      value = get_sum_answers_point(i)
    }
    display_circle_points(i, value);
  }
}

function get_sum_answers_point(criteria_id){
  $answers = $jq("[name^='criteria_question-"+criteria_id+"-']")
  total = 0;
  $answers.each(function(i, v){
    value = v.value;
    if(value == "none") { value = 0 }
    else{ value = parseFloat(value) }
    total = total + value
  });
  return total;
}
