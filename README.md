# ObradorsCompartits.cat

## Introducció

Aquest es el software lliure creat amb python/Django per a l'eina [ObradorsCompartits.cat](https://obradorscompartits.cat)

## Instalació
Versió Postgres recomendada:
 PostgreSQL 10 i PostGIS 2.5

```
- Instalar Postgis: 
     sudo docker run --name postgis-instance -e POSTGRES_PASSWORD=docker -p 25432:5432 -d postgis/postgis:10-2.5
- Descargar repository git
cd obradorscompartits
mkvirtualenv obradorscompartits
cp local_template.py local.py 
#Modificar el local.py amb les contrasenyes i claus privades.

#Instalar dependencies python
pip install -r requirements.txt
sudo locale-gen ca_ES.utf8
sudo update-locale
python manage.py runserver
```

## Llicència
Affero GNU GPL-v3